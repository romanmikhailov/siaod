program KR2;
type
    TreeValue = Integer;
    TreeNode = record
        left: ^TreeNode;
        right: ^TreeNode;
        parent: ^TreeNode;
        value: TreeValue;
        rightIsThread: Boolean;
    end;
    TreeNodeP = ^TreeNode;
    Tree = record
        root: TreeNodeP;
    end;
    TreeP = ^Tree;

    ListValue = TreeNodeP;
    ListNode = record
        next: ^ListNode;
        prev: ^ListNode;
        value: Pointer;
    end;
    ListNodeP = ^ListNode;
    List = record
        head: ListNodeP;
        tail: ListNodeP;
    end;
    ListP = ^List;

    StringP = ^String;

    TTraversalHelper = procedure (var node: TreeNodeP; var list: ListP);

procedure Error(message:String);
begin
    WriteLn('Error: ' + message);
    halt(1);
end; { Error }

{ List function defenitions }

function CreateList():ListP;
var
    list:ListP;
begin
    new(list);
    list^.head := nil;
    list^.tail := nil;
    CreateList := list;
end; { CreateList }

function CreateListNode(var value:Pointer):ListNodeP;
var
    node: ListNodeP;
begin
    new(node);
    node^.next := nil;
    node^.prev := nil;
    node^.value := value;
    CreateListNode := node;
end; { CreateListNode }

procedure ListInsert(var value:Pointer; var list:ListP);
var
    node: ListNodeP;
begin
    if nil = list
    then begin
        Error('ListInsert. List is nil!');
    end;

    node := CreateListNode(value);

    if nil = list^.head
    then begin
        node^.prev := nil;
        list^.head := node;
        list^.tail := node;
    end
    else begin
        node^.prev := list^.tail;
        list^.tail^.next := node;
        list^.tail := node;

    end;
end; { ListInsert }

function ListNodeToString(var listNode:ListNodeP):String;
    var res:String;
    var value:ListValue;
begin
    value := ListValue(listNode^.value);
    if nil = value
    then begin
        res := 'nil';
    end
    else begin
        Str(value^.value, res);
    end;
    ListNodeToString := res;
end; { ListNodeToString }

function ListToString(var list:ListP):String;
var
    node: ^ListNode;
    isNotFirst: Boolean;
    res: String;
begin
    res := '';
    isNotFirst := false;
    node := list^.head;
    while node <> nil do
    begin
        if isNotFirst
        then begin
            res := res + ' -> ';
        end;
        res := res + ListNodeToString(node);
        isNotFirst := true;
        node := node^.next;
    end;
    ListToString := res;
end; { ListToString }

procedure PrintList(var list:ListP);
begin
    WriteLn(ListToString(list));
end; { PrintList }

procedure DisposeList(var list:ListP);
var
    node, next:ListNodeP;
begin
    node := list^.head;
    while nil <> node do
    begin
        next := node^.next;
        Dispose(node);
        node := next;
    end;
    Dispose(list);
end; { DisposeList }

{List functions definitions end}

{Tree functions definitions}

function CreateTree():TreeP;
var
    tr: TreeP;
begin
    new(tr);
    tr^.root := nil;
    CreateTree := tr;
end; { CreateTree }

function CreateTreeNode(value:TreeValue):TreeNodeP;
var
    node:TreeNodeP;
begin
    new(node);
    node^.left := nil;
    node^.right := nil;
    node^.value := value;
    node^.parent := nil;
    node^.rightIsThread := false;
    CreateTreeNode := node;
end; { CreateTreeNode }

procedure TreeInsert(var value:TreeValue; var tree:TreeP);
var
    node, insertingNode, oldThreadNode:TreeNodeP;
begin
    insertingNode := CreateTreeNode(value);

    if nil = tree^.root
    then begin
        tree^.root := insertingNode;
        tree^.root^.parent := nil;
        Exit();
    end;

    node := tree^.root;
    while True do
    begin
        if node^.value > value
        then begin
            if nil = node^.left
            then begin
                node^.left := insertingNode;
                insertingNode^.parent := node;
                break;
            end;
            node := node^.left;
        end
        else if node^.value < value
        then begin
            if (nil = node^.right) OR (node^.rightIsThread)
            then begin
                oldThreadNode := node^.right;
                node^.right := insertingNode;
                node^.rightIsThread := false;
                insertingNode^.parent := node;
                if nil <> oldThreadNode
                then begin
                    insertingNode^.rightIsThread := true;
                    insertingNode^.right := oldThreadNode;
                end;
                break;
            end;
            node := node^.right;
        end
        else begin
            { Ingore case when pasting value is equal to exist. }
            Dispose(insertingNode);
            break;
        end;
    end;
end; { TreeInsert }

function StringifiedListToTree(str:String):TreeP;
var
    c: Char;
    i, value, code: Integer;
    buf: String;
    t: TreeP;
begin
    t := CreateTree();
    buf := '';
    for i := 1 to Integer(str[0]) do
    begin
        if ' ' = str[i]
        then begin
            if 0 <> Integer(buf[0])
            then begin
                Val(buf, value, code);
                if 0 = code
                then begin
                    TreeInsert(value, t);
                end;
            end;
            buf := '';
        end
        else begin
            buf := buf + str[i];
        end;
    end;
    if 0 <> Integer(buf[0])
    then begin
        Val(buf, value, code);
        if 0 = code
        then begin
            TreeInsert(value, t);
        end;
    end;
    StringifiedListToTree := t;
end; { StringifiedListToTree }

procedure DisposeSubTree(var node:TreeNodeP);
begin
    if nil = node
    then begin
        Exit();
    end;
    DisposeSubTree(node^.left);
    if NOT node^.rightIsThread
    then begin
        DisposeSubTree(node^.right);
    end;
    Dispose(node);
end; { DisposeSubTree }

procedure DisposeTree(var t:TreeP);
var
    node: TreeNodeP;
begin
    if nil = t
    then begin
        Exit();
    end;
    DisposeSubTree(t^.root);
    Dispose(t);
end; { DisposeTree }

function TreeNodeToString(var node:TreeNodeP):String;
var
    res:String;
begin
    if nil = node
    then begin
        res := 'nil';
    end
    else begin
        Str(node^.value, res);
    end;
    TreeNodeToString := res;
end; { TreeNodeToString }

function SubTreeToString(var node:TreeNodeP; indent:String):String;
const
    LINE_END = #13#10;
    LEFT_INDENT = #179'  ';
    RIGHT_INDENT = '   ';
    LAST_INDENT_LEFT = #195#196#196;
    LAST_INDENT_RIGHT = #192#196#196;
var
    levelIntent, str: String;
    isRight, isRoot, hasChildren: Boolean;
begin
    isRoot := false;
    hasChildren := false;
    isRight := false;
    if nil <> node
    then begin
        hasChildren := (nil <> node^.left) OR (nil <> node^.right);
        isRoot := nil = node^.parent;
        if NOT isRoot
        then isRight := node^.parent^.right = node;
    end;

    if isRoot
    then
        levelIntent := ''
    else if NOT isRight
        then levelIntent := LAST_INDENT_LEFT
        else levelIntent := LAST_INDENT_RIGHT;

    str := indent + levelIntent + TreeNodeToString(node) + LINE_END;

    if NOT isRoot
    then begin
        if isRight
        then indent := indent + RIGHT_INDENT
        else indent := indent + LEFT_INDENT;
    end;

    if NOT hasChildren
    then begin
        SubTreeToString := str;
        Exit;
    end;

    str := str + SubTreeToString(node^.left, indent);

    if NOT node^.rightIsThread
    then str := str + SubTreeToString(node^.right, indent)
    else str := str + indent + LAST_INDENT_RIGHT + '{' + TreeNodeToString(node^.right) + '}' + LINE_END;

    SubTreeToString := str;
end; { SubTreeToString }

procedure PrintTree(var tree:TreeP);
var
    start: TreeNodeP;
begin
    if nil = tree
    then begin
        start := nil;
    end
    else begin
        start := tree^.root;
    end;
    Write(SubTreeToString(start, ''));
end; { PrintTree }

procedure _PreOrderTraversalHelper(var node:TreeNodeP; var currentList:ListP);
begin
    if nil = node
    then begin
        Exit();
    end;
    ListInsert(node, currentList);
    _PreOrderTraversalHelper(node^.left, currentList);
    if NOT node^.rightIsThread
    then begin
        _PreOrderTraversalHelper(node^.right, currentList);
    end;
end; { _PreOrderTraversalHelper }

procedure _InOrderTraversalHelper(var node:TreeNodeP; var currentList:ListP);
begin
    if nil = node
    then begin
        Exit();
    end;
    _InOrderTraversalHelper(node^.left, currentList);
    ListInsert(node, currentList);
    if NOT node^.rightIsThread
    then begin
        _InOrderTraversalHelper(node^.right, currentList);
    end;
end; { _InOrderTraversalHelper }

procedure _PostOrderTraversalHelper(var node:TreeNodeP; var currentList:ListP);
begin
    if nil = node
    then begin
        Exit();
    end;
    _PostOrderTraversalHelper(node^.left, currentList);
    if NOT node^.rightIsThread
    then begin
        _PostOrderTraversalHelper(node^.right, currentList);
    end;
    ListInsert(node, currentList);
end; { _PostOrderTraversalHelper }

function GetTreeTraversalList(var tree:TreeP; traversalHelper: TTraversalHelper):ListP;
    var res:ListP;
begin
    res := CreateList();
    if nil <> tree
    then begin
        traversalHelper(tree^.root, res);
    end;
    GetTreeTraversalList := res;
end; {GetTreeTraversalList}

function GetTreeTraversalListString(var tree:TreeP; traversalHelper:TTraversalHelper):String;
var
    list: ListP;
begin
    list := GetTreeTraversalList(tree, traversalHelper);
    GetTreeTraversalListString := ListToString(list);
    DisposeList(list);
end; { GetTreeTraversalListString }

procedure PrintTreeTraversals(var tree:TreeP);
begin
    Write('Pre-Order Traversal  : ');
    WriteLn(GetTreeTraversalListString(tree, @_PreOrderTraversalHelper));
    Write('In-Order Traversal   : ');
    WriteLn(GetTreeTraversalListString(tree, @_InOrderTraversalHelper));
    Write('Post-Order Traversal : ');
    WriteLn(GetTreeTraversalListString(tree, @_PostOrderTraversalHelper));
end; { PrintTreeTraversals }

procedure ThreadTree(var t:TreeP);
var
    l: ListP;
    node: ListNodeP;
    treeNode, nextTreeNode : TreeNodeP;
begin
    l := GetTreeTraversalList(t, @_InOrderTraversalHelper);
    if nil = l then node := nil else node := l^.head;
    while nil <> node do
    begin
        treeNode := TreeNodeP(node^.value);
        if (nil = treeNode^.right) OR (treeNode^.rightIsThread)
        then begin
            treeNode^.rightIsThread := true;
            if nil <> node^.next
            then begin
                nextTreeNode := TreeNodeP(node^.next^.value);
            end
            else begin
                nextTreeNode := nil;
            end;
            treeNode^.right := nextTreeNode;
        end
        else begin
            treeNode^.rightIsThread := false;
        end;
        node := node^.next;
    end;
    DisposeList(l);
end;

procedure DebugTree(s:String);
var
    t:TreeP;
begin
    WriteLn('Input integers list: ' + s);
    t := StringifiedListToTree(s);
    WriteLn('Tree:');
    PrintTree(t);
    WriteLn('Tree traversals:');
    PrintTreeTraversals(t);
    ThreadTree(t);
    WriteLn('Threaded tree:');
    PrintTree(t);
    DisposeTree(t);
end; { DebugTree }

{Tree functions definitions end}

begin
    DebugTree('13 2 43 54 5 1');
    DebugTree('4 2 1 3 6 5 7');
    DebugTree('4 1 5 3 6 7 1 2 8 9');
    DebugTree('4 3 8 1 6 9 2 5 7');
    ReadLn;
end.





















