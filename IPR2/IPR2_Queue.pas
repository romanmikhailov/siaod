program IPR2;

uses sysutils, crt;

type
    TPointerToString = function(value:Pointer): String;
    TComparePredicate = function(left: Pointer; right: Pointer): Integer;
    TPointerAction = procedure(value:Pointer);

    ListNode = record
        next: ^ListNode;
        prev: ^ListNode;
        value: Pointer;
    end;
    ListNodeP = ^ListNode;
    List = record
        head: ListNodeP;
        tail: ListNodeP;
        valueToString: TPointerToString;
        comparePredicate: TComparePredicate;
        valueDispose: TPointerAction;
    end;
    ListP = ^List;

    PInteger = ^Integer;

{ Common utils subroutines }

procedure Error(msg:String);
begin
    WriteLn('Error: ' + msg);
    Halt(1);
end;

{ Common utils subroutines end }

{ List subroutines begin }

function ListNodeCreate(value:Pointer):ListNodeP;
var node:ListNodeP;
begin
    new(node);
    node^.next := nil;
    node^.prev := nil;
    node^.value := value;
    ListNodeCreate := node;
end; { ListNodeCreate }

procedure ListNodeDispose(var node:ListNodeP);
begin
    Dispose(node);
end; { ListNodeDispose }

function ListCreate(valueToString: TPointerToString; comparePredicate: TComparePredicate; valueDispose: TPointerAction):ListP;
var list:ListP;
begin
    new(list);
    list^.valueToString := valueToString;
    list^.comparePredicate := comparePredicate;
    list^.valueDispose := valueDispose;
    list^.head := nil;
    list^.tail := nil;
    ListCreate := list;
end; { ListCreate }

procedure ListDispose(var list:ListP);
var node, temp: ListNodeP;
begin
    if nil = list
    then Exit;

    node := list^.head;
    while nil <> node do
    begin
        temp := node^.next;
        list^.valueDispose(node^.value);
        ListNodeDispose(node);
        node := temp;
    end;

    Dispose(list);
end; { ListDispose }

procedure ListInsert(var list:ListP; value:Pointer);
var node, it:ListNodeP;
begin
    if nil = list
    then Error('ListInsert. List is nil!');

    node := ListNodeCreate(value);

    if nil = list^.head
    then begin
        list^.head := node;
        list^.tail := node;
        Exit;
    end;
    it := list^.head;
    while nil <> it do
    begin
        if list^.comparePredicate(it^.value, value) > 0
        then break;
        it := it^.next;
    end;

    if list^.head = list^.tail
    then begin
        if nil = it
        then begin
            list^.tail := node;
            list^.head^.next := node;
            node^.prev := list^.head;
        end
        else begin
            list^.head := node;
            node^.next := list^.tail;
            list^.tail^.prev := node;
        end;
    end
    else begin
        if it = nil
        then begin
            list^.tail^.next := node;
            node^.prev := list^.tail;
            list^.tail := node;
        end
        else if it = list^.head
        then begin
            list^.head^.prev := node;
            node^.next := list^.head;
            list^.head := node;
        end
        else begin
            node^.prev := it^.prev;
            node^.next := it;
            it^.prev^.next := node;
            it^.prev := node;
        end;
    end;
end; { ListInsert }

function ListToString(var list:ListP):String;
var
    node: ListNodeP;
    res : String;
    isNotFirst: Boolean;
begin
    if nil = list
    then node := nil
    else node := list^.head;

    if nil = list^.valueToString
    then Error('ListToString. valueToString helper is nil.');

    isNotFirst := false;

    res := '';

    while nil <> node do
    begin
        if isNotFirst
        then res := res + ', ';

        res := res + list^.valueToString(node^.value);

        isNotFirst := true;

        node := node^.next;
    end;

    ListToString := res;
end; { ListToString }

procedure ListPrint(var list:ListP);
begin
    WriteLn(ListToString(list));
end; { ListPrint }

procedure ListDelete(var list: ListP; node:ListNodeP);
var
    temp: ListNodeP;
begin
    if nil = list
    then Error('ListDelete. list in nil!');

    if nil = node
    then Exit;

    if (node = list^.head) AND (node = list^.tail)
    then begin
        list^.head := nil;
        list^.tail := nil;
    end
    else if node = list^.head
    then begin
        list^.head := list^.head^.next;
        list^.head^.prev := nil;
    end
    else if node = list^.tail
    then begin
        list^.tail := list^.tail^.prev;
        list^.tail^.next := nil;
    end
    else begin
        node^.prev^.next := node^.next;
        node^.next^.prev := node^.prev;
    end;

    list^.valueDispose(node^.value);
    ListNodeDispose(node);
end; { ListDelete }

{ List subtoutines end }

{ Waiting queue subroutines }

type
    WaitingQueue = record
        queue: ListP;
    end;
    WaitingQueueP = ^WaitingQueue;

    Process = record
        timeLeft: Integer;
        priority: Integer;
        name: String;
    end;
    ProcessP = ^Process;

var waiting: WaitingQueueP;

function ProcessCreate(name:String; needTime:Integer; priority: Integer):ProcessP;
var
    process: ProcessP;
    counterP: ^Integer;
begin
    new(process);
    process^.timeLeft := needTime;
    process^.priority := priority;
    process^.name := name;
    ProcessCreate := process;
end; { ProcessCreate }

function _ProcessToString(process:Pointer):String;
var
    p: ProcessP;
    priority, timeLeft: String;
begin
    p := ProcessP(process);
    Str(p^.priority, priority);
    Str(p^.timeLeft, timeLeft);
    _ProcessToString := '{name: "' + p^.name + '", priority: ' + priority + ', time: ' + timeLeft + '}';
end; { _ProcessToString }

function _ComparePriorities(left: Pointer; right: Pointer):Integer;
begin
    _ComparePriorities := ProcessP(left)^.priority - ProcessP(right)^.priority
end; { _ComparePriorities }

procedure _DisposeProcess(process: Pointer);
begin
    Dispose(ProcessP(process));
end; { _DisposeProcess }

procedure WaitingInitialize();
begin
    new(waiting);
    waiting^.queue := ListCreate(@_ProcessToString, @_ComparePriorities, @_DisposeProcess);
end; { WaitingInitialize }

procedure WaitingDispose();
begin
    if nil = waiting
    then Exit;

    ListDispose(waiting^.queue);
    Dispose(waiting);
    waiting := nil;
end; { WaitingDispose }

procedure Insert(process: ProcessP);
begin
    ListInsert(waiting^.queue, process);
end; { Insert }

function DeleteMin():ProcessP;
var
    node: ListNodeP;
    process:ProcessP;
    pCopy: ProcessP;
begin
    process := nil;
    node := waiting^.queue^.head;
    if nil <> node
    then begin
        process := node^.value;
        new(pCopy);
        pCopy^.timeLeft := process^.timeLeft;
        pCopy^.priority := process^.priority;
        pCopy^.name := process^.name;
        process := pCopy;
    end;
    ListDelete(waiting^.queue, node);
    DeleteMin := process;
end; { DeleteMin }

procedure Select();
const
    ITERATIONS = 3;
    ITERATION_TIME = 50;
var
    process:ProcessP;
    i:Integer;
    tmp:String;
begin
    WriteLn;
    Write('Waiting queue: ');
    ListPrint(waiting^.queue);

    process := DeleteMin();
    if (nil = process)
    then begin
        WriteLn('Execution list is empty. Waiting for new job.');
        Exit;
    end;
    Str(process^.priority, tmp);
    WriteLn('Manage new quantum. Start process: ' + process^.name + ' with priority ' + tmp);
    for i := 1 to ITERATIONS do
    begin
        WriteLn('Execute process: ' + process^.name);
        dec(process^.timeLeft, ITERATION_TIME);
        if 0 >= process^.timeLeft
        then begin
            WriteLn('Process (' + process^.name + ') finished before quantum.');
            Break;
        end;
{        Sleep(ITERATION_TIME);}
    end;

    if process^.timeLeft > 0
    then begin
        inc(process^.priority, 100);
        str(process^.priority, tmp);
        WriteLn('Process (' + process^.name + ') not finished. Return to waiting with priority: ' + tmp);
        Insert(process);
    end
    else begin
        WriteLn('Process (' + process^.name + ') finished.');
    end;

{    Sleep(5000); }
    Select();
end; { Select }

function GenerateProcess():ProcessP;
var
    process: ProcessP;
    name:String;
    r:String;
begin
    str(random(1000), r);
    name := 'pr_' + r;
    process := ProcessCreate(name, random(5000), (random(20) + 1) * 100);
    GenerateProcess := process;
end; { GenerateProcess }

{ Waiting queue subroutines end }

begin
    randomize;
    WaitingInitialize();
    Insert(ProcessCreate('pr_1', 600, 50));
    Insert(ProcessCreate('pr_2', 400, 70));
    Insert(ProcessCreate('pr_3', 200, 150));
    Select();
    WaitingDispose();
    ReadLn;
end.
