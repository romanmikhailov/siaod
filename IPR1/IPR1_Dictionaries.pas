program IPR1;
uses sysutils;
type
    TPointerToString = function(value:Pointer): String;
    TPointerPredicate = function(value:Pointer; context:Pointer): Boolean;
    TComparePredicate = function(left: Pointer; right: Pointer): Integer;

    ListNode = record
        next: ^ListNode;
        prev: ^ListNode;
        value: Pointer;
    end;
    ListNodeP = ^ListNode;
    List = record
        head: ListNodeP;
        tail: ListNodeP;
        valueToString: TPointerToString;
    end;
    ListP = ^List;

    DictionaryValue = record
        key: String;
        value: String;
    end;
    Dictionary = record
        list: ListP;
    end;
    DictionaryValueP = ^DictionaryValue;
    DictionaryP = ^Dictionary;

{ Common utils subroutines }

procedure Error(msg:String);
begin
    WriteLn('Error: ' + msg);
    Halt(1);
end;

{ Common utils subroutines end }

{ List subroutines begin }

function ListNodeCreate(value:Pointer):ListNodeP;
var node:ListNodeP;
begin
    new(node);
    node^.next := nil;
    node^.prev := nil;
    node^.value := value;
    ListNodeCreate := node;
end; { ListNodeCreate }

procedure ListNodeDispose(var node:ListNodeP);
begin
    Dispose(node);
end; { ListNodeDispose }

function ListCreate(valueToString: TPointerToString):ListP;
var list:ListP;
begin
    new(list);
    list^.valueToString := valueToString;
    list^.head := nil;
    list^.tail := nil;
    ListCreate := list;
end; { ListCreate }

procedure ListDispose(var list:ListP);
var node, temp: ListNodeP;
begin
    if nil = list
    then Exit;

    node := list^.head;
    while nil <> node do
    begin
        temp := node^.next;
        ListNodeDispose(node);
        node := temp;
    end;

    Dispose(list);
end; { ListDispose }

procedure ListInsert(var value:Pointer; var list:ListP);
var node:ListNodeP;
begin
    if nil = list
    then Error('ListInsert. List is nil!');

    node := ListNodeCreate(value);

    if nil = list^.head
    then begin
        list^.head := node;
        list^.tail := node;
    end
    else begin
        node^.prev := list^.tail;
        list^.tail^.next := node;
        list^.tail := node;
    end;
end; { ListInsert }

function ListToString(var list:ListP):String;
var
    node: ListNodeP;
    res : String;
    isNotFirst: Boolean;
begin
    if nil = list
    then node := nil
    else node := list^.head;

    if nil = list^.valueToString
    then Error('ListToString. valueToString helper is nil.');

    isNotFirst := false;

    res := '';

    while nil <> node do
    begin
        if isNotFirst
        then res := res + ', ';

        res := res + list^.valueToString(node^.value);

        isNotFirst := true;

        node := node^.next;
    end;

    ListToString := res;
end; { ListToString }

function ListFind(var list: ListP; findPredicate: TPointerPredicate; predicateContext: Pointer):ListNodeP;
var node: ListNodeP;
begin
    ListFind := nil;
    if nil = list
    then Exit;

    node := list^.head;

    while nil <> node do
    begin
        if findPredicate(node^.value, predicateContext)
        then begin
            ListFind := node;
            Break;
        end;
        node := node^.next;
    end;
end; { ListFind }

procedure ListPrint(var list:ListP);
begin
    WriteLn(ListToString(list));
end; { ListPrint }

procedure ListDelete(var list: ListP; node:ListNodeP);
var
    temp: ListNodeP;
begin
    if nil = list
    then Error('ListDelete. list in nil!');

    if nil = node
    then Exit;

    if (node = list^.head) AND (node = list^.tail)
    then begin
        list^.head := nil;
        list^.tail := nil;
    end
    else if node = list^.head
    then begin
        list^.head := list^.head^.next;
        list^.head^.prev := nil;
    end
    else if node = list^.tail
    then begin
        list^.tail := list^.tail^.prev;
        list^.tail^.next := nil;
    end
    else begin
        node^.prev^.next := node^.next;
        node^.next^.prev := node^.prev;
    end;

    ListNodeDispose(node);
end; { ListDelete }

procedure ListSort(var list: ListP; comparePredicate: TComparePredicate);
var
    iNode, jNode: ListNodeP;
    temp: Pointer;
begin
    if nil = list then Exit;

    iNode := list^.head;
    while nil <> iNode do
    begin
        jNode := iNode^.next;
        while nil <> jNode do
        begin
            if comparePredicate(iNode^.value, jNode^.value) > 0
            then begin
                temp := iNode^.value;
                iNode^.value := jNode^.value;
                jNode^.value := temp;
            end;
            jNode := jNode^.next;
        end;
        iNode := iNode^.next;
    end;
end; { ListSort }

{ List subtoutines end }

{ Dictionary subroutines begin }

function DictionaryValueToString(value:Pointer):String;
var
    dictValue: DictionaryValueP;
    res: String;
begin
    if nil = value
    then res := 'nil'
    else begin
        dictValue := DictionaryValueP(value);
        if nil = dictValue
        then res := 'nil'
        else res := '{' + dictValue^.key + ': ' + dictValue^.value + '}';
    end;
    DictionaryValueToString := res;
end; { DictionaryValueToString }

function DictionaryValueCreate(var key:String; var value:String):DictionaryValueP;
var dictValue: DictionaryValueP;
begin
    new(dictValue);
    dictValue^.key := key;
    dictValue^.value := value;
    DictionaryValueCreate := dictValue;
end; { DictionaryValueCreate }

procedure DictionaryValueDispose(var value: DictionaryValueP);
begin
    if nil = value
    then Exit;

    Dispose(value);
end; { DictionaryValueDispose }

function DictionaryCreate():DictionaryP;
var
    dictionary: DictionaryP;
begin
    new(dictionary);
    dictionary^.list := ListCreate(@DictionaryValueToString);
    DictionaryCreate := dictionary;
end; { DictionartCreate }

procedure DictionaryDispose(var dictionary:DictionaryP);
begin
    if nil = dictionary
    then Exit;
    ListDispose(dictionary^.list);
    Dispose(dictionary);
end; { DictionaryDispose }

function _FindPairByKeyPredicate(current:Pointer; context: Pointer):Boolean;
type
    StringP = ^String;
var
    needKey: String;
    dictValue: DictionaryValue;
begin
    needKey := StringP(context)^;
    dictValue := DictionaryValueP(current)^;
    _FindPairByKeyPredicate := needKey = dictValue.key;
end; { _FindPairByKeyPredicate }

procedure DictionaryPut(var dictionary:DictionaryP; key:String; value:String);
    var node: ListNodeP;
    var dictValue: DictionaryValueP;
begin
    if nil = dictionary
    then Error('DictionaryPut. Dictionary is nil');

    node := ListFind(dictionary^.list, @_FindPairByKeyPredicate, @key);

    if nil <> node
    then begin
        dictValue := node^.value;
        dictValue^.key := key;
        dictValue^.value := value;
    end
    else begin
        dictValue := DictionaryValueCreate(key, value);
        ListInsert(dictValue, dictionary^.list);
    end;
end; { DictionaryPut }

procedure DictionaryDelete(var dictionary:DictionaryP; key:String);
    var node: ListNodeP;
begin
    if nil = dictionary
    then Error('DictionaryDelete. Dictionary is nil');

    node := ListFind(dictionary^.list, @_FindPairByKeyPredicate, @key);
    if nil <> node
    then DictionaryValueDispose(node^.value);

    ListDelete(dictionary^.list, node);
end; { DictionaryDelete }

function DictionaryGet(var dictionary:DictionaryP; key:String):String;
    var node: ListNodeP;
begin
    if nil = dictionary
    then Error('DictionaryGet. Dictionary is nil');

    node := ListFind(dictionary^.list, @_FindPairByKeyPredicate, @key);

    if nil <> node
    then DictionaryGet := DictionaryValueP(node^.value)^.value
    else Error('DictionaryGet. Requested key (' + key + ') not found.');
end; { DictionaryGet }

function DictionaryHas(var dictionary:DictionaryP; key:String):Boolean;
begin
    if nil = dictionary
    then Error('DictionaryGet. Dictionary is nil');

    DictionaryHas := nil <> ListFind(dictionary^.list, @_FindPairByKeyPredicate, @key);
end; { DictionaryHas }

function DictionaryToString(var dictionary:DictionaryP):String;
begin
    if nil = dictionary
    then DictionaryToString := 'nil'
    else DictionaryToString := '[' + ListToString(dictionary^.list) + ']';
end; { DictionaryToString }

procedure DictionaryPrint(var dictionary:DictionaryP);
begin
    WriteLn(DictionaryToString(dictionary));
end; { DictionaryPrint }

function _KeysComparePredicate(left: Pointer; right: Pointer):Integer;
begin
    _KeysComparePredicate := CompareStr(DictionaryValueP(left)^.key, DictionaryValueP(right)^.key);
end; { _KeysComparePredicate }

function _ValuesComparePredicate(left: Pointer; right: Pointer):Integer;
begin
    _ValuesComparePredicate := CompareStr(DictionaryValueP(left)^.value, DictionaryValueP(right)^.value);
end; { _ValuesComparePredicate }

procedure DictionarySortByKeys(var dictionary: DictionaryP);
begin
    if nil = dictionary then Exit;
    ListSort(dictionary^.list, @_KeysComparePredicate);
end; { DictionarySortByKeys }

procedure DictionarySortByValues(var dictionary: DictionaryP);
begin
    if nil = dictionary then Exit;
    ListSort(dictionary^.list, @_ValuesComparePredicate);
end; { DictionarySortByValues }

{ Dictionary subroutines end }

var
    dict: DictionaryP;
begin
    Write('Create empty dictionary: ');
    dict := DictionaryCreate();
    DictionaryPrint(dict);

    Write('Add    : Set value "value4" for key "key1": ');
    DictionaryPut(dict, 'key1', 'value4');
    DictionaryPrint(dict);

    Write('Add    : Set value "value3" for key "key2": ');
    DictionaryPut(dict, 'key2', 'value3');
    DictionaryPrint(dict);

    Write('Add    : Set value "value7" for key "key4": ');
    DictionaryPut(dict, 'key4', 'value7');
    DictionaryPrint(dict);

    Write('Search : Is value for key "key3" exist? ');
    WriteLn(DictionaryHas(dict, 'key3'));

    Write('Search : Is value for key "key2" exist? ');
    WriteLn(DictionaryHas(dict, 'key2'));

    Write('Get    : Value for key "key2": ');
    WriteLn(DictionaryGet(dict, 'key2'));

    Write('Delete : Delete value for key "key2": ');
    DictionaryDelete(dict, 'key2');
    DictionaryPrint(dict);

    Write('Search : Is value for key "key2" exist? ');
    WriteLn(DictionaryHas(dict, 'key2'));

    Write('Edit   : Set value "value5" for key "key1": ');
    DictionaryPut(dict, 'key1', 'value5');
    DictionaryPrint(dict);

    Write('Add    : Set value "value1" for key "key3": ');
    DictionaryPut(dict, 'key3', 'value1');
    DictionaryPrint(dict);

    Write('Sort   : By keys: ');
    DictionarySortByKeys(dict);
    DictionaryPrint(dict);

    Write('Sort   : By values: ');
    DictionarySortByValues(dict);
    DictionaryPrint(dict);

    DictionaryDispose(dict);
    ReadLn;
end.
