program KR1;

const
    MAX_LENGTH = 1000;
    STACK_BOTTOM = '_';
    OP_OPEN_BR =   '(';
    OP_CLOSE_BR =  ')';
    OP_PLUS =      '+';
    OP_MINUS =     '-';
    OP_MULT =      '*';
    OP_DIVIDE =    '/';
    OP_POWER =     '^';
    OP_ERROR =     '!';
    OP_NUM =       '@';
    OP_LETTER =    '$';
    OP_WHITESPACE =' ';
type
    Item = record
        value: integer;
        isOperator: boolean;
        priority: byte;
    end; { Item }
    Expression = record
        elements: array[1..MAX_LENGTH] of Item;
        count: integer;
    end;
    Stack = record
        top: integer;
        elements: array[1..MAX_LENGTH] of Item;
    end; { Stack }

procedure PrintError(const s:String);
begin
    Write('Error: ');
    WriteLn(s);
    Halt(1);
end; { PrintError }

procedure ClearStack(var s:Stack);
begin
    s.top := MAX_LENGTH + 1;
end; { ClearStack }

function IsStackEmpty(var s:Stack):Boolean;
begin
   IsStackEmpty := s.top = (MAX_LENGTH + 1);
end; { IsStackEmpty }

function TopStack(s:Stack): Item;
begin
    if IsStackEmpty(s)
    then begin
        PrintError('Stack is empty');
    end
    else begin
        TopStack := s.elements[s.top];
    end;
end; { TopStack }

function PopStack(var s:Stack):Item;
begin
    if IsStackEmpty(s)
    then begin
        PrintError('Empty stack');
    end
    else begin
        PopStack := s.elements[s.top];
        s.top := s.top + 1;
    end;
end; { PopStack }

procedure StackPush(x: Item; var s:Stack);
begin
    if s.top = 1
    then begin
        PrintError('Stack is full');
    end
    else begin
        dec(s.top);
        s.elements[s.top] := x;
    end
end; { StackPush }

function GetPriority(c:Char):Byte;
begin
    case c of
        STACK_BOTTOM: GetPriority                 := 0;
        OP_OPEN_BR: GetPriority                   := 1;
        OP_CLOSE_BR: GetPriority                  := 2;
        OP_PLUS, OP_MINUS: GetPriority            := 3;
        OP_MULT, OP_DIVIDE, OP_POWER: GetPriority := 4;
        OP_NUM: GetPriority                       := 5;
        OP_LETTER: GetPriority                    := 6
        else GetPriority                          := 7;
    end;
end;

function ItemToString(it:Item):String;
var
    s:String;
begin

    if (GetPriority(OP_LETTER) = it.priority) OR it.isOperator
    then begin
        ItemToString := '' + Char(it.value);
    end
    else begin
        Str(it.value, s);
        ItemToString := s;
    end;
end; { ItemToString }


procedure PrintItem(it:Item);
begin
    Write(ItemToString(it));
end; { PrintItem }

function IsLetter(s:String):Boolean;
begin
    IsLetter := (Integer(s[0]) = 1) AND (((s[1] >= 'A') AND (s[1] <= 'Z')) OR ((s[1] >= 'a') AND (s[1] <= 'z')));
end;

function ConvertToItem(s:String):Item;
var
    c: Char;
    value, errorCode: Integer;
    it: Item;
begin
    c := s[1];
    it.isOperator := ((STACK_BOTTOM = c)
        OR (OP_OPEN_BR = c)
        OR (OP_CLOSE_BR = c)
        OR (OP_PLUS = c)
        OR (OP_MINUS = c)
        OR (OP_MULT = c)
        OR (OP_DIVIDE = c)
        OR (OP_POWER = c)
        OR (OP_WHITESPACE = c)) AND (1 = byte(s[0]));
    if it.isOperator
    then begin
        it.value := Integer(c);
        it.priority := GetPriority(c);
    end
    else begin
        if (IsLetter(s))
        then begin
            it.isOperator := false;
            it.value := Integer(s[1]);
            it.priority := GetPriority(OP_LETTER);
        end
        else begin
            Val(s, value, errorCode);
            if 0 <> errorCode
            then begin
                it.isOperator := true;
                it.value := Integer(OP_ERROR);
            end
            else begin
                it.isOperator := false;
                it.value := value;
                it.priority := GetPriority(OP_NUM);
            end;
        end;
    end;
    ConvertToItem := it;
end; {ConvertToItem}

function IsDigit(c:Char):Boolean;
begin
    IsDigit := (c >= '0') AND (c <= '9');
end; { IsDigit }

function ExpressionToString(expr:Expression):String;
var
    res: String;
    i: Integer;
begin
    res := '';
    for i := 1 to expr.count do
    begin
        res := res + ItemToString(expr.elements[i]);
        if i <> expr.count
        then res := res + ' ';
    end;
    ExpressionToString := res;
end; { ExpressionToString }

procedure PrintExpression(expr:Expression);
begin
    WriteLn(ExpressionToString(expr));
end; { PrintExpression }

procedure PrintStack(s:Stack);
var
    i: Integer;
begin
    for i := MAX_LENGTH downto s.top do
    begin
        PrintItem(s.elements[i]);
        Write(' ');
    end;
    WriteLn;
end; { PrintStack }

function ConvertToExpression(s:String):Expression;
var
    currNum, st: String;
    it: Item;
    i: Integer;
    expr: Expression;
begin
    currNum := '';
    expr.count := 1;
    for i:=1 to Integer(s[0]) do
    begin
        if IsDigit(s[i])
        then begin
            currNum := currNum + s[i];
        end
        else begin
            if 0 <> Integer(currNum[0])
            then begin
                expr.elements[expr.count] := ConvertToItem(currNum);
                currNum := '';
                inc(expr.count);
            end;
            it := ConvertToItem(s[i]);
            if Integer(OP_WHITESPACE) = it.value
            then begin
                continue;
            end;
            if Integer(OP_ERROR) = it.value
            then begin
                Str(i, st);
                PrintError('Incorrect char: ' + s[i] + ' at position: ' + st);
            end;
            expr.elements[expr.count] := it;
            inc(expr.count);
        end;
    end;
    if 0 <> Integer(currNum[0])
    then begin
        expr.elements[expr.count] := ConvertToItem(currNum);
        currNum := '';
        inc(expr.count);
    end;
    dec(expr.count);
    ConvertToExpression := expr;
end; {ConvertToExpression}

function ConvertToReversePolishNotation(source:Expression):Expression;
var
    i: Integer;
    res: Expression;
    it, sTop: Item;
    s: Stack;
begin
    ClearStack(s);
    StackPush(ConvertToItem(STACK_BOTTOM), s);
    res.count := 0;
    for i := 1 to source.count do
    begin
        it := source.elements[i];
        sTop := TopStack(s);
        if (1 = it.priority) OR (it.priority > sTop.priority)
        then begin
            StackPush(it, s);
        end
        else begin
            while (NOT IsStackEmpty(s)) AND (TopStack(s).priority >= it.priority) do
            begin
                sTop := PopStack(s);
                if (OP_OPEN_BR <> Char(sTop.value)) AND (OP_CLOSE_BR <> Char(sTop.value))
                then begin
                    inc(res.count);
                    res.elements[res.count] := sTop;
                end;
            end;
            if (NOT IsStackEmpty(s)) AND (Char(TopStack(s).value) = OP_OPEN_BR) AND (Char(it.value) = OP_CLOSE_BR)
            then begin
                PopStack(s);
            end
            else begin
                StackPush(it, s);
            end;
        end;
    end;
    while TopStack(s).priority <> 0 do
    begin
        sTop := PopStack(s);
        if (OP_OPEN_BR <> Char(sTop.value)) AND (OP_CLOSE_BR <> Char(sTop.value))
        then begin
            inc(res.count);
            res.elements[res.count] := sTop;
        end;
    end;
    ConvertToReversePolishNotation := res;
end; { ConvertToReversePolishNotation }

{
    Translate source user input expression into Expression,
      convert into Reverse Polish Notation and compare result
      string representation of expression with expected.
    @param source - Input string.
    @param expected - Expected string.
}
procedure CheckConverting(source:String; expected:String);
var
    result: String;
begin
    result := ExpressionToString(
        ConvertToReversePolishNotation(
            ConvertToExpression(source)
        )
    );
    WriteLn('Source expression  : ' + source);
    WriteLn('Expected expression: ' + expected);
    WriteLn('Result expression  : ' + result);
    if result = expected
    then WriteLn('Correct!')
    else WriteLn('NOT CORRECT!');
    WriteLn;
end; { CheckConverting }

begin
    CheckConverting('a+b', 'a b +');
    CheckConverting('a+b+c', 'a b + c +');
    CheckConverting('a+(b+c)', 'a b c + +');
    CheckConverting('a+b*c', 'a b c * +');
    CheckConverting('a*(b+c)', 'a b c + *');
    CheckConverting('a*b*c', 'a b * c *');
    CheckConverting('A + B * C - D / E * H', 'A B C * + D E / H * -');
    CheckConverting('a + b * c - d / (a + b)', 'a b c * + d a b + / -');
    CheckConverting('((A - (B + C)) * D) ^ (E + F)', 'A B C + - D * E F + ^');
    CheckConverting('1+31^2*(54+6^(8-7))', '1 31 2 ^ 54 6 8 7 - ^ + * +');

    ReadLn;
end.
